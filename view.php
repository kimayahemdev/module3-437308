<?php
session_start();
require_once 'config.php';

 $results = mysqli_query($link, "SELECT title, username, storyid FROM story");
 ?>
<style>
	.edit_btn {
    text-decoration: none;
    padding: 2px 4px;
    background: #2E8B57;
    color: white;
    border-radius: 3px;
}
#file {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#filetd, #file th {
    border: 1px solid #ddd;
    padding: 8px;
}

#file tr:nth-child(even){background-color: #f2f2f2;}

#file tr:hover {background-color: #ddd;}

#file th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}

</style>
<table id='file'>
	<thead>
		<tr>
			<th>id</th>
			<th>title</th>
			<th>username</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	
	<?php while ($row = mysqli_fetch_array($results)) { ?>
		<tr>
			<td><?php echo $row['storyid']; ?></td>
			<td><?php echo $row['title']; ?></td>
			<td><?php echo $row['username']; ?></td>
			<td>
				<a href="comment.php?edit=<?php echo $row['storyid']; ?>" class="edit_btn" >Comment</a>
			</td>
			
		</tr>
	<?php } ?>
</table>

<form>



